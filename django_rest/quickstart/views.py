from django.shortcuts import render
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from rest_framework import viewsets, permissions
from django_rest.quickstart.serializers import UserSerializer, GroupSerializer

# Custom user defined by cookiecutter
User = get_user_model()

# ViewSets group together common behaviour for different views inside classes
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]
