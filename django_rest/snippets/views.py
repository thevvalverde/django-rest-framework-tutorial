from django.contrib.auth import get_user_model
from rest_framework import generics, permissions, renderers
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from django_rest.snippets.models import Snippet
from django_rest.snippets.permissions import IsOwnerOrReadOnly
from django_rest.snippets.serializers import SnippetSerializer, UserSerializer

User = get_user_model()


class UserListView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SnippetListView(generics.ListCreateAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class SnippetDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


class SnippetHighlightView(generics.GenericAPIView):
    queryset = Snippet.objects.all()
    renderer_classes = [renderers.StaticHTMLRenderer]

    def get(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)


class APIRootView(APIView):
    def get(self, request, format=None):
        return Response(
            {
                "users": reverse("user-list", request=request, format=format),
                "snippets": reverse("snippet-list", request=request, format=format),
            }
        )
